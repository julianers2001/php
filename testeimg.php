<?php
require_once('lib/controle/ImgControle.class.php');
$comando = new ImgControle();
define('TAMANHO_MAXIMO', (2 * 1024 * 1024));
if (!isset($_FILES['img'])){
    echo "<script>
    alert('Img invalida');
    window.location = ('formadmin.php');
     </script>";
}else{
$foto = $_FILES['img'];
$nome = $foto['name'];
$tipo = $foto['type'];
$tamanho = $foto['size'];
}
if(!preg_match('/^image\/(pjpeg|jpeg|png|gif|bmp)$/', $tipo)){
    echo "<script>
    alert('Img invalida');
    window.location = ('formadmin.php');
     </script>";
}else if($tamanho > TAMANHO_MAXIMO){
    echo "<script>
    alert('Img invalida');
    window.location = ('formadmin.php');
     </script>";
}else{
$conteudo = file_get_contents($foto['tmp_name']);
$comando->adicionarImg($conteudo,$tipo,$tamanho,$nome);
  header("Location:formadmin.php");

}
?>