<?php 
session_start();
require_once('../lib/controle/Controlelogin.class.php');
$login = new ControleLogin();

require_once('../lib/controle/ControleTudo.class.php');
$comando = new ControleTudo();
$a = $comando->consultaHPage();
$b = $comando->consultaHPageId(count($a));
echo"<!doctype html>
  <head>
    <meta charset='utf-8'>
    <a href='https://icons8.com'></a>
    <title>Arco e flecha</title>
";
foreach($b as $item){
echo"
    <!-- Bootstrap-->
    <link href='../bootstrap-4.0.0/dist/css/bootstrap.min.css' rel='stylesheet'>
    <!-- Custom styles for this template -->
    <link href='../css/cover.css' rel='stylesheet'>
    <script src='//code.jquery.com/jquery-2.1.1.min.js'></script>
    <link rel='stylesheet' href='jquery.preloader-1.2.css'>
    <script src='jquery.preloader-1.2.js'></script> 
    <style>
        body{
          background-image:url('../img/home.jpg');
        }
        h1{
          color:red;
        }
        p,h2{
          color:black;
          }
        </style>
  </head>

  <body>

    <div class='cover-container d-flex h-100 p-3 mx-auto flex-column text-center'>
    ";
    if (!isset($_SESSION['email'])) {
    echo"
      <header class='masthead mb-auto'>
        <div class='inner'>
          <h6 class='masthead-brand'><a href='../loginadmin.php'><img src='../img/flechab2.png'>Fazer login</a></h6>
        </div>
      </header>
";
}else{
 echo" <header class='masthead mb-auto'>
        <div class='inner'>
          <h6 class='masthead-brand'><a href='../sair.php'><img src='../img/flechab2.png'>Sair</a></h6>
        </div>
      </header>
      ";
}
echo"
      <main role='main' class='inner cover text-center '>
        <h1 class='display-1'><img src='../img/sz4.png'> 
        {$item->getNome()}
        <img src='../img/seta2.png'></h1>
        <h2 class='display-6'>{$item->getTexto()}</h2>
        <p class='lead'>
          <a href='index2.php' class='btn btn-lg btn-secondary'>{$item->getBotao()}<img src='../img/flechap.png' height='30' width='30'></a>
        </p>";
        if (isset($_SESSION['email'])) {
          $d = $login->consultaLogin();
          $c = $login->consultaLoginId(count($d));
          foreach($c as $item){
                if($_SESSION['email']==$item->getUser()){
                    echo"
                       <center><a href='../formadmin.php' class='btn btn-lg btn-primary'>Editar</a></center>";
                }
              }
            }
              echo"
      </main>

      <footer class='mastfoot mt-auto'>
        <div class='inner'>
        <p>Feito com Bootstrap por Juliane. </br>&copy;2018</p>
        </div>
      </footer>
    </div>
    <script src='../bootstrap-4.0.0/assets/js/vendor/popper.min.js'></script>
    <script src='../bootstrap-4.0.0/dist/js/bootstrap.min.js'></script>
  </body>
</html>
";
}
?>