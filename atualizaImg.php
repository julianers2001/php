<?php
require_once("lib/controle/ImgControle.class.php");
$comando = new ImgControle();
// Constantes
define('TAMANHO_MAXIMO', (2 * 1024 * 1024));
$id= $_POST['idimg'];
$foto = $_FILES['imge'];
$nome = $foto['name'];
$tipo = $foto['type'];
$tamanho = $foto['size'];
if(!preg_match('/^image\/(pjpeg|jpeg|png|gif|bmp)$/', $tipo)){
    echo "<script>
    alert('Img invalida');
    window.location = ('formadmin.php');
     </script>";
}
if ($tamanho > TAMANHO_MAXIMO){
    echo "<script>
    alert('Img invalida');
    window.location = ('formadmin.php');
     </script>";
}
$conteudo = file_get_contents($foto['tmp_name']);
$comando->atualizaImg($id,$conteudo,$tipo,$tamanho,$nome);
  header("Location:formadmin.php");
?>