<?php
    class Login{
        private $id;
        private $user;
        private $pwd;
        //encapsulamento
        public function getId(){
            return $this->id;
        }
        public function setId($id){
            $this->id = ($id != NULL) ? addslashes($id) : NULL;
        }
        public function getUser(){
            return $this->user;
        }
        public function setUser($user){
            $this->user = ($user != NULL) ? addslashes($user) : NULL;
        }
        public function getPwd(){
            return $this->pwd;
        }
        public function setPwd($pwd){
            $this->pwd = ($pwd != NULL) ? addslashes($pwd) : NULL;
        }
    } 
?>