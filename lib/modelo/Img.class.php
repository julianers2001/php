<?php
    class Img{
        private $id;
        private $conteudo;
        private $tipo;    
        private $tamanho;
        private $nome;
        //encapsulamento
        public function getId(){
            return $this->id;
        }
        public function setId($id){
            $this->id = ($id != NULL) ? addslashes($id) : NULL;
        }
        public function getConteudo(){
            return $this->conteudo;
        }
        public function setConteudo($conteudo){
            $this->conteudo = ($conteudo != NULL) ? $conteudo : NULL;
        }
        public function getTipo(){
            return $this->tipo;
        }
        public function setTipo($tipo){
            $this->tipo = ($tipo != NULL) ? addslashes($tipo) : NULL;
        }
        public function getTamanho(){
            return $this->tamanho;
        }
        public function setTamanho($tamanho){
            $this->tamanho = ($tamanho != NULL) ? addslashes($tamanho) : NULL;
        }
        public function getNome(){
            return $this->nome;
        }
        public function setNome($nome){
            $this->nome = ($nome != NULL) ? addslashes($nome) : NULL;
        }
    } 
?>