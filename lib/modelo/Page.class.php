<?php
    class Page{
        private $id;
        private $textoimg;
        private $textonome;
        //encapsulamento
        public function getId(){
            return $this->id;
        }
        public function setId($id){
            $this->id = ($id != NULL) ? addslashes($id) : NULL;
        }
        public function getTextoimg(){
            return $this->textoimg;
        }
        public function setTextoimg($ti){
            $this->textoimg = ($ti != NULL) ? addslashes($ti) : NULL;
        }
        public function getTextonome(){
            return $this->textonome;
        }
        public function setTextonome($textonome){
            $this->textonome = ($textonome != NULL) ? addslashes($textonome) : NULL;
        }
    } 
?>