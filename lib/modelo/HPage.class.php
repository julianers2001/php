<?php
    class HPage{
        private $nome;
        private $id;
        private $botao;
        private $texto;
        //encapsulamento
        public function getNome(){
            return $this->nome;
        }
        public function setNome($n){
            $this->nome = ($n != NULL) ? addslashes($n) : NULL;
        }
        public function getId(){
            return $this->id;
        }
        public function setId($id){
            $this->id = ($id != NULL) ? addslashes($id) : NULL;
        }
        public function getBotao(){
            return $this->botao;
        }
        public function setBotao($b){
            $this->botao = ($b != NULL) ? addslashes($b) : NULL;
        }
        public function getTexto(){
            return $this->texto;
        }
        public function setTexto($texto){
            $this->texto = ($texto != NULL) ? addslashes($texto) : NULL;
        }
    } 
?>