<?php
    require_once("lib/Conexao.class.php");
    require_once("lib/modelo/Loginadmin.class.php");
    final class Controlelogin{
        public function consultaLogin(){
            $conexao = new Conexao("confi/confi.ini");
            $comando = $conexao->getConexao()->prepare("SELECT * FROM login");
            $comando->execute();
            $resu = $comando->fetchAll();
            $lista = array();
            foreach($resu as $item){
                $login = new login();
                $login->setId($item->id);
                $login->setUser($item->user);
                $login->setPwd($item->pwd);
                array_push($lista, $login);
            }
            $conexao->__destruct();
            return $lista;
        }
        public function consultaLoginId($id){
        $conexao = new Conexao("confi/confi.ini");
        $comando = $conexao->getConexao()->prepare("SELECT * FROM login where id= :id");
        $comando->bindValue(":id",$id);
        $comando->execute();
        $resu = $comando->fetchAll();
        $lista = array();
        foreach($resu as $item){
            $login = new Login();
            $login->setId($item->id);
            $login->setUser($item->user);
            $login->setPwd($item->pwd);
            array_push($lista, $login);
        }
        $conexao->__destruct();
        return $lista;
    }
    public function atualizaLogin($user, $pwd){
        $conexao = new Conexao("confi/confi.ini");
        $up = $conexao->getConexao()->prepare("UPDATE login SET user =:user, pwd = :pwd");
        $up->bindValue(":user", $user);
        $up->bindValue(":pwd", $pwd);
        if($up->execute()){
            $conexao->__destruct();
            return true;
        }else{
            $conexao->__destruct();
            return false;
        }
    }
    }


?>