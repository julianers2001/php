<?php
require_once("lib/Conexao.class.php");
require_once("lib/modelo/HPage.class.php");
final class ControleTudo{
    public function consultaHPage(){
        $conexao = new Conexao("confi/confi.ini");
        $comando = $conexao->getConexao()->prepare("SELECT * FROM homepage");
        $comando->execute();
        $resu = $comando->fetchAll();
        $lista = array();
        foreach($resu as $item){
            $hPage = new HPage();
            $hPage->setId($item->id);
            $hPage->setNome($item->nome);
            $hPage->setBotao($item->botao);
            $hPage->setTexto($item->texto);
            array_push($lista, $hPage);
        }
        $conexao->__destruct();
        return $lista;
    }
    public function consultaHPageId($id){
        $conexao = new Conexao("confi/confi.ini");
        $comando = $conexao->getConexao()->prepare("SELECT * FROM homepage where id= :id");
        $comando->bindValue(":id",$id);
        $comando->execute();
        $resu = $comando->fetchAll();
        $lista = array();
        foreach($resu as $item){
            $hPage = new HPage();
            $hPage->setId($item->id);
            $hPage->setNome($item->nome);
            $hPage->setBotao($item->botao);
            $hPage->setTexto($item->texto);
            array_push($lista, $hPage);
        }
        $conexao->__destruct();
        return $lista;
    }
    public function adicionarHPage($HPage){
        $conexao = new Conexao("../confi/confi.ini");
        $sql = "INSERT INTO homepage(nome, botao, texto) VALUES (:no,:bo,:te)";
        $comando = $conexao->getConexao()->prepare($sql);
        $comando->bindValue(":no",$HPage->getNome());
        $comando->bindValue(":bo",$HPage->getBotao());
        $comando->bindValue(":te",$HPage->getTexto());
        if($comando->execute()){
            $conexao->__destruct();
            return true;
        }else{
            $conexao->__destruct();
            return false;
        }
    }
    public function atualizaHPage($id, $nome, $texto, $botao){
        $conexao = new Conexao("confi/confi.ini");
        $up = $conexao->getConexao()->prepare("UPDATE homepage SET nome =:nome, texto = :texto, botao = :botao WHERE id=:id");
        $up->bindValue(":nome", $nome);
        $up->bindValue(":botao", $botao);
        $up->bindValue(":texto", $texto);
        $up->bindValue(":id", $id);
        if($up->execute()){
            $conexao->__destruct();
            return true;
        }else{
            $conexao->__destruct();
            return false;
        }
    }
    public function deletaHPage($id){
        $conexao = new Conexao("confi/confi.ini");
        $del = $conexao->getConexao()->prepare("DELETE FROM homepage WHERE id=:id");
        $del->bindValue(":id",$id);
        if($del->execute()){
            $conexao->__destruct();
            return true;
        }else{
            $conexao->__destruct();
            return false;
        }
    }   
}
?>