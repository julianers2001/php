<?php
require_once("lib/Conexao.class.php");
require_once("lib/modelo/CuriosidadesListas.class.php");
final class ListaControle{
public function consultaclista(){
        $conexao = new Conexao("confi/confi.ini");
        $comando = $conexao->getConexao()->prepare("SELECT * FROM curiosidadeslistas");
        $comando->execute();
        $resu = $comando->fetchAll();
        $lista = array();
        foreach($resu as $item){
            $clista = new CuriosidadesListas();
            $clista->setId($item->id);
            $clista->setTexto($item->texto);
            $clista->setNome($item->nome);
            array_push($lista, $clista);
        }
        $conexao->__destruct();
        return $lista;
    }
    public function atualizaclistas($id,$texto,$nome){
        $conexao = new Conexao("confi/confi.ini");
        $up = $conexao->getConexao()->prepare("UPDATE curiosidadeslistas SET texto = :texto, nome=:nome WHERE id=:id");
        $up->bindValue(":texto", $texto);
        $up->bindValue(":nome", $nome);
        $up->bindValue(":id", $id);
        if($up->execute()){
            $conexao->__destruct();
            return true;
        }else{
            $conexao->__destruct();
            return false;
        }
    }
    public function adicionarclistas($comentario){
        $conexao = new Conexao("confi/confi.ini");
        $sql = "INSERT INTO curiosidadeslistas(nome,texto) VALUES (:nome,:texto)";
        $comando = $conexao->getConexao()->prepare($sql);
        $comando->bindValue(':nome',$comentario->getNome());
        $comando->bindValue(':texto',$comentario->getTexto());
        if($comando->execute()){
                $conexao->__destruct();
                return true;
            }else{
                $conexao->__destruct();
                return false;
            }
    }
    public function deletaclistas($id){
        $conexao = new Conexao("confi/confi.ini");
        $del = $conexao->getConexao()->prepare("DELETE FROM curiosidadeslistas WHERE id=:id");
        $del->bindValue(":id",$id);
        if($del->execute()){
            $conexao->__destruct();
            return true;
        }else{
            $conexao->__destruct();
            return false;
        }
    }
}
?>