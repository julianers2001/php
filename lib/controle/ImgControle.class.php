<?php
require_once("lib/Conexao.class.php");
require_once("lib/modelo/Img.class.php");
final class ImgControle{
    public function adicionarImg($conteudo,$tipo,$tamanho,$nome){
        $conexao = new Conexao("confi/confi.ini");
        $sql = 'INSERT INTO fotos (conteudo, tipo, tamanho, nome) VALUES (:conteudo, :tipo, :tamanho ,:nome)';
        $comando = $conexao->getConexao()->prepare($sql);
        $comando->bindParam(':conteudo', $conteudo, PDO::PARAM_LOB);
        $comando->bindParam(':tipo', $tipo, PDO::PARAM_STR);
        $comando->bindParam(':tamanho', $tamanho, PDO::PARAM_INT);
        $comando->bindParam(':nome', $nome, PDO::PARAM_STR);
        $comando->execute();
        $conexao->__destruct();
    }
    public function consultaImgId($id){
        $conexao = new Conexao("confi/confi.ini");
        $comando = $conexao->getConexao()->prepare("SELECT conteudo, tipo FROM fotos WHERE id = :id");
        $comando->bindParam(':id', $id, PDO::PARAM_INT);
        if ($comando->execute()){
            $foto = $comando->fetchObject();
            if ($foto != null){
                header('Content-Type: '. $foto->tipo);
                echo $foto->conteudo;
            }
        }
        $conexao->__destruct();
    }
    public function deletaImg($id){
        $conexao = new Conexao("confi/confi.ini");
        $del = $conexao->getConexao()->prepare("DELETE FROM fotos WHERE id=:id");
        $del->bindValue(":id",$id);
        if($del->execute()){
            $conexao->__destruct();
            return true;
        }else{
            $conexao->__destruct();
            return false;
        }
    }
    public function atualizaImg($id,$conteudo, $tipo, $tamanho, $nome){
        $conexao = new Conexao("confi/confi.ini");
        $comando = $conexao->getConexao()->prepare("UPDATE fotos SET nome =:nome, tamanho = :tamanho, tipo = :tipo, conteudo = :conteudo WHERE id=:id");
        $comando->bindParam(':conteudo', $conteudo, PDO::PARAM_LOB);
        $comando->bindParam(':tipo', $tipo, PDO::PARAM_STR);
        $comando->bindParam(':tamanho', $tamanho, PDO::PARAM_INT);
        $comando->bindParam(':nome', $nome, PDO::PARAM_STR);
        $comando->bindParam(':id', $id, PDO::PARAM_INT);
        if($comando->execute()){
            $conexao->__destruct();
            return true;
        }else{
            $conexao->__destruct();
            return false;
        }
    }
}
?>