<?php
require_once('lib/Conexao.class.php');
require_once('lib/modelo/Curiosidades.class.php');
final class CuriosidadesControle{
public function consultacuriosidades(){
        $conexao = new Conexao("confi/confi.ini");
        $comando = $conexao->getConexao()->prepare("SELECT * FROM curiosidades");
        $comando->execute();
        $resu = $comando->fetchAll();
        $lista = array();
        foreach($resu as $item){
            $Cur = new Curiosidades();
            $Cur->setId($item->id);
            $Cur->setNome($item->nome);
            $Cur->setTitulol($item->titulol);
            $Cur->setTextonome($item->textonome);
            array_push($lista, $Cur);
        }
        $conexao->__destruct();
        return $lista;
    }
    public function atualizacuriosidades($id,$textonome,$titulol){
        $conexao = new Conexao("confi/confi.ini");
        $up = $conexao->getConexao()->prepare("UPDATE curiosidades SET textonome = :textonome, titulol=:titulol WHERE id=:id");
        $up->bindValue(":textonome", $textonome);
        $up->bindValue(":titulol", $titulol);
        $up->bindValue(":id", $id);
        if($up->execute()){
            $conexao->__destruct();
            return true;
        }else{
            $conexao->__destruct();
            return false;
        }
    }
    public function adicionarcuriosidades(){
        $conexao = new Conexao("confi/confi.ini");
        $sql = "INSERT INTO curiosidades(textonome,titulol) VALUES ('teste','teste')";
        $comando = $conexao->getConexao()->prepare($sql);
        $comando->execute();
        $conexao->__destruct();
    }
    public function deletacuriosidades($id){
        $conexao = new Conexao("confi/confi.ini");
        $del = $conexao->getConexao()->prepare("DELETE FROM curiosidades WHERE id=:id");
        $del->bindValue(":id",$id);
        if($del->execute()){
            $conexao->__destruct();
            return true;
        }else{
            $conexao->__destruct();
            return false;
        }
    }
}
?>