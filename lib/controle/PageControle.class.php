<?php
require_once("lib/Conexao.class.php");
require_once("lib/modelo/Page.class.php");
final class PageControle{
public function consultaPage(){
        $conexao = new Conexao("confi/confi.ini");
        $comando = $conexao->getConexao()->prepare("SELECT * FROM page");
        $comando->execute();
        $resu = $comando->fetchAll();
        $lista = array();
        foreach($resu as $item){
            $Page = new Page();
            $Page->setId($item->id);
            $Page->setTextoimg($item->textoimg);
            $Page->setTextonome($item->textonome);
            array_push($lista, $Page);
        }
        $conexao->__destruct();
        return $lista;
    }
    public function consultaPageId($id){
        $conexao = new Conexao("confi/confi.ini");
        $comando = $conexao->getConexao()->prepare("SELECT * FROM page where id= :id");
        $comando->bindValue(":id",$id);
        $comando->execute();
        $resu = $comando->fetchAll();
        $lista = array();
        foreach($resu as $item){
            $Page = new Page();
            $Page->setId($item->id);
            $Page->setTextonome($item->textonome);
            $Page->setTextoimg($item->textoimg);
            array_push($lista, $Page);
        }
        $conexao->__destruct();
        return $lista;
    }
    public function adicionarPage($Page){
        $conexao = new Conexao("confi/confi.ini");
        $sql = "INSERT INTO page(textoimg, textonome) VALUES (:ti,:te)";
        $comando = $conexao->getConexao()->prepare($sql);
        $comando->bindValue(":ti",$Page->getTextoimg());
        $comando->bindValue(":te",$Page->getTextonome());
        if($comando->execute()){
            $conexao->__destruct();
            return true;
        }else{
            $conexao->__destruct();
            return false;
        }
    }
    public function atualizaPage($id, $textoimg, $textonome){
        $conexao = new Conexao("confi/confi.ini");
        $up = $conexao->getConexao()->prepare("UPDATE page SET textoimg = :textoimg, textonome = :textonome WHERE id=:id");
        $up->bindValue(":textoimg", $textoimg);
        $up->bindValue(":textonome", $textonome);
        $up->bindValue(":id", $id);
        if($up->execute()){
            $conexao->__destruct();
            return true;
        }else{
            $conexao->__destruct();
            return false;
        }
    }
    public function deletaPage($id){
        $conexao = new Conexao("confi/confi.ini");
        $del = $conexao->getConexao()->prepare("DELETE FROM page WHERE id=:id");
        $del->bindValue(":id",$id);
        if($del->execute()){
            $conexao->__destruct();
            return true;
        }else{
            $conexao->__destruct();
            return false;
        }
    }
}



?>