<?php
require_once("lib/Conexao.class.php");
require_once("lib/modelo/Video.class.php");
final class VideoControle{
	public function adicionarVideo($video){
        $tipo = $video->getTipo();
        $videoC=file_get_contents($video->getConteudo());
        $conexao = new Conexao("confi/confi.ini");
        $sql = 'INSERT INTO video (conteudo, tipo) VALUES (:conteudo, :tipo)';
        $comando = $conexao->getConexao()->prepare($sql);
        $comando->bindParam(':conteudo', $videoC);
        $comando->bindParam(':tipo', $tipo);
        if($comando->execute()){
            $conexao->__destruct();
            return true;
        }else{
            $conexao->__destruct();
            return false;
        }
    }
    public function deletaVideo($id){
            $conexao = new Conexao("confi/confi.ini");
            $del = $conexao->getConexao()->prepare("DELETE FROM video WHERE id=:id");
            $del->bindValue(":id",$id);
            if($del->execute()){
                $conexao->__destruct();
                return true;
            }else{
                $conexao->__destruct();
                return false;
            }
    }
    public function atualizaVideo($id,$video){
        $tipo = $video->getTipo();
        $videoC=file_get_contents($video->getConteudo());
        $conexao = new Conexao("confi/confi.ini");
        $comando = $conexao->getConexao()->prepare("UPDATE video SET tipo = :tipo, conteudo = :conteudo WHERE id=:id");        
        $comando->bindParam(':conteudo', $videoC);
        $comando->bindParam(':tipo', $tipo);
        $comando->bindParam(':id', $id);

        if($comando->execute()){
            $conexao->__destruct();
            return true;
        }else{
            $conexao->__destruct();
            return false;
        }
    }
}

?>