<?php
session_start();
if (!isset($_SESSION['email'])) {
echo "
<!doctype html>
<html lang='en'>
  <head>
    <meta charset='utf-8'>
    <title>Login</title>
    <link href='bootstrap-4.0.0/dist/css/bootstrap.min.css' rel='stylesheet'>
    <link href='css/loginadmin.css' rel='stylesheet'>
    <link href='css/nav2.css' rel='stylesheet'>
  </head>

  <body class='text-center'>
<ul class='nav'>
<li><a href='front/index.php'>Voltar<img class='bg-img' src='img/flechab.png' width='30' height='30'></a>
</ul>  
    <form action='fazerlogin.php' method='post' class='form-signin'>
    <img src='img/flechap.png'/>
      <h1>Bem-vindo ao seu site</h1>

      <label for='email' class='sr-only'>Login:</label>
      <input type='inputEmail' name='email' id='email' class='form-control' placeholder='Seu email' required autofocus>

      <label for='pwd' class='sr-only'>Senha:</label>
      <input type='password' name='pwd' id='pwd' class='form-control' placeholder='Sua senha' required>
 
      <button class='btn btn-lg btn-primary btn-block' type='submit'>Fazer Login</button>
   <a href='novaSenha.php'>Esqueceu sua senha?</a>
 </form>
  </body>
</html>
";
}else{
  header('Location: front/index.php');
}
?>