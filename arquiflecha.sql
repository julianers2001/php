CREATE SCHEMA if not exists usuario;
CREATE TABLE if not exists usuario.login(
	id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	user VARCHAR(15) NOT NULL,
	pwd VARCHAR(8) NOT NULL
);
INSERT INTO usuario.login (user,pwd) VALUES("admin@admin.com","qwe123");
CREATE TABLE if not exists usuario.homepage(
	id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	nome VARCHAR(15) NOT NULL,
	texto VARCHAR(200) NOT NULL,
	botao VARCHAR(50) NOT NULL
);
INSERT INTO usuario.homepage (nome,texto,botao) VALUES ("Arquiflecha","Arcos e Flechas","Venha flechar conosco");
CREATE TABLE if not exists usuario.page(
	id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	textonome VARCHAR(200) NOT NULL,
	textoimg VARCHAR(200) NOT NULL
);
INSERT INTO usuario.page (textonome,textoimg) VALUES ("Flechas e arcos","Curiosidades");
CREATE TABLE if not exists usuario.curiosidades(
	id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	nome VARCHAR(15) NOT NULL,
	textonome VARCHAR(1000) NOT NULL,
	titulol VARCHAR(1000) NOT NULL
);
INSERT INTO usuario.curiosidades (nome,textonome,titulol) VALUES ("Arcos","Flechas","arco e flechas");
CREATE TABLE if not exists usuario.curiosidadeslistas(
	id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	nome VARCHAR(200) NOT NULL,
	texto VARCHAR(1000) NOT NULL
);
CREATE TABLE if not exists usuario.fotos (
 id int(10) unsigned NOT NULL AUTO_INCREMENT,
 nome varchar(60) NOT NULL,
 conteudo mediumblob NOT NULL,
 tipo varchar(20) NOT NULL,
 tamanho int(10) unsigned NOT NULL,
 PRIMARY KEY (id)
);
CREATE TABLE if not exists usuario.video (
 id int(10) unsigned NOT NULL AUTO_INCREMENT,
 conteudo longblob NOT NULL,
 tipo varchar(20) NOT NULL,
 PRIMARY KEY (id)
);